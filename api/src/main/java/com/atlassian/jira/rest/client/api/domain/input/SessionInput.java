package com.atlassian.jira.rest.client.api.domain.input;

import com.google.common.base.MoreObjects;

public class SessionInput {
	
	private final String username;
	private final String password;
	
	
	public SessionInput(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("username", username)
				.add("password", password)
				.toString();
	}
	
}
